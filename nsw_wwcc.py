import os
from selenium import webdriver
import pandas as pd
from time import sleep
import sys
from random import uniform

DELAY = 0.5
DRIVER = None

MONTHS = {
	"01": "January",
	"02": "February",
	"03": "March",
	"04": "April",
	"05": "May",
	"06": "June",
	"07": "July",
	"08": "August",
	"09": "September",
	"10": "October",
	"11": "November",
	"12": "December",
}

def pause(delay):
	sleep(delay + uniform(-delay / 2, delay / 2))

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def send_text(ele, text, delay=0.05):
	if delay < 10e-5:
		ele.send_keys(text)
		return

	for char in text:
		ele.send_keys(char)
		pause(delay + uniform(-delay / 2, delay / 2))

def start_driver():
	global DRIVER
	DRIVER = webdriver.Chrome(resource_path('./driver/chromedriver.exe'))

def nsw_login(usr, pwd):
	DRIVER.get("https://wwccheck.ccyp.nsw.gov.au/Employers/Login")
	send_text(DRIVER.find_element_by_id("Username"), usr)
	pause(DELAY)
	send_text(DRIVER.find_element_by_id("Password"), pwd) 
	pause(DELAY)
	DRIVER.find_element_by_id("Login").click()

	return not "error" in DRIVER.page_source

def nsw_enter_data(staff):
	num = len(staff[:10])
	for i in range(num):
		send_text(DRIVER.find_element_by_id(f"Criteria_{i}__FamilyName"), staff.iloc[i]["Last Name"], 0)
		pause(DELAY)
		send_text(DRIVER.find_element_by_id(f"Criteria_{i}__BirthDate"), staff.iloc[i]["DOB"], 0)
		pause(DELAY)
		send_text(DRIVER.find_element_by_id(f"Criteria_{i}__AuthorisationNumber"), staff.iloc[i]["WWC NSW #"], 0)
		pause(DELAY)
		if i < num - 1:
			DRIVER.find_element_by_id("Add").click()
			pause(DELAY * 2)

def nsw(usr, pwd, staff):
	staff, ret = check_format(staff, "nsw")
	if not ret: 
		return False
	login_pass = nsw_login(usr, pwd)
	if not login_pass:
		print('failed')
		DRIVER.quit()
		return False

	nsw_enter_data(staff)
	return True

def check_format(staff, state):
	staff = staff.dropna()
	return staff, True

def date_parse(vals, f, t, delimeter):
	if f == "ymd" and t == "dmy" or f == "dmy" and t == "ymd":
		return f"{delimeter}".join(vals[::-1])

def run_nsw_checker(filename, usr, pwd):
	start_driver()

	if filename.endswith(".csv"):
		staff = pd.read_csv(filename)
	else:
		staff = pd.read_excel(filename)
	
	staff = staff[["Last Name", "Dob", "WWC NSW #", "WWC NSW Expiry Date"]]
	staff["Dob"] = staff["Dob"].fillna('1990-01-01')
	staff["DOB-Y"] = staff["Dob"].map(lambda x: str(x).split(" ")[0].split("-")[0])
	staff["DOB-M"] = staff["Dob"].map(lambda x: str(x).split(" ")[0].split("-")[1])
	staff["DOB-D"] = staff["Dob"].map(lambda x: str(x).split(" ")[0].split("-")[2])
	staff["DOB"] = staff["Dob"].map(lambda x: date_parse(str(x).split(" ")[0].split("-"), "ymd", "dmy", "/"))

	staff["Expiry"] = staff["WWC NSW Expiry Date"].fillna('1990-01-01')
	staff["Expiry"] = staff["Expiry"].map(lambda x: str(x).split(" ")[0].split("-")[2] + "/" + str(x).split(" ")[0].split("-")[1] + "/" + str(x).split(" ")[0].split("-")[0])

	return nsw(usr, pwd, staff)

def change_delay(delay):
	global DELAY
	DELAY = delay
	print(DELAY)