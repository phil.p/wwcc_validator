from tkinter import *
import tkinter.font as font
from tkinter import filedialog, Text, END
import json
import nsw_wwcc
import os

SUCCESS = 0
WARNING = 1
ERROR = 2
REMOVE = 3

CHECKED = False
DELAY = 0.5
FILENAME = None
error_label = None
delay_label = None

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def save(window, usr, pwd):
	save_data(usr.get(), pwd.get(), False)
	window.after(5000, lambda: save(window, usr, pwd))

def add_file(window):
	global FILENAME

	try:
		filename = filedialog.askopenfilename(initialdir="/", title="Select File", 
										  filetypes=(("Excel Sheets", "*.xlsx"), ("CSVs", "*.csv")))
		if filename is None or len(filename) < 1:
			raise Exception("No file given")
		
		FILENAME = filename
		show_msg(window, SUCCESS, "File added!")
	except Exception:
		show_msg(window, ERROR, "Error: no file added")

def get_data():
	global CHECKED
	usr = None
	pwd = None

	try:
		f = open("save.dat")
	except FileNotFoundError:
		return None, None

	data = json.load(f)
	f.close()

	CHECKED = data['checked'] == 'True'
	usr = data['usr']
	pwd = data['pwd']

	return usr, pwd

def save_data(usr, pwd, change=True):
	global CHECKED

	if change:
		CHECKED = not CHECKED 

	f = open("save.dat", "w")

	data = json.dumps({
		'checked': 'True' if CHECKED else 'False',
		'usr': usr if CHECKED else '',
		'pwd': pwd if CHECKED else ''
	})
	f.write(data)

	f.close()

def show_msg(window, err_type, msg=None):
	global error_label
	font = ('Cuprum', 12)

	error_label.destroy()

	if err_type == SUCCESS:
		error_label = Label(window, text=msg, bg="#EEEEEE", fg="#59FF59", font=font)
	elif err_type == WARNING:
		error_label = Label(window, text=msg, bg="#EEEEEE", fg="#FFD600", font=font)
	elif err_type == ERROR:
		error_label = Label(window, text=msg, bg="#EEEEEE", fg="#FF5959", font=font)
	else:
		return

	error_label.place(x=400, y=540, anchor="center")

def set_delay(window, element):
	global DELAY
	global delay_label

	if element is None:
		return

	if element:
		delay = element.get()
		element.delete(0, len(delay))
	else:
		delay = new_delay

	try:
		if len(delay) < 1:
			raise TypeError("No value entered")
		delay = float(delay)
	except TypeError:
		return show_msg(window, ERROR, "Error: no number was entered")
	except ValueError:
		return show_msg(window, ERROR, "Error: delay must be a number")

	warned = False
	if delay > 30:
		return show_msg(window, ERROR, "Error: delay too long")
	elif delay < 0:
		return show_msg(window, ERROR, "Error: delay must be positive")
	elif delay > 2:
		warned = True
		show_msg(window, WARNING, "Warning: long delay")
	elif delay < 10e-5:
		warned = True
		show_msg(window, WARNING, "Warning: short delay")

	if not warned:
		show_msg(window, SUCCESS, "Delay has been changed")

	DELAY = delay
	nsw_wwcc.change_delay(delay)

	delay_label.destroy()

	delay_label = Label(window, text= f"Delay set to {delay:.1f} sec", font=('Cuprum', 10), fg = "#343434")
	delay_label.place(x=550, y=390)

def run(window, usr, pwd):
	if not FILENAME:
		return show_msg(window, ERROR, "Error: no file has been selected")
	if not usr:
		return show_msg(window, ERROR, "Error: no username entered")
	if not pwd:
		return show_msg(window, ERROR, "Error: no password entered")

	show_msg(window, REMOVE)

	if not nsw_wwcc.run_nsw_checker(FILENAME, usr, pwd):
		show_msg(window, ERROR, "Error: Login credentials were incorrect")


def main():
	global CHECKED
	global delay_label
	global error_label

	usr, pwd = get_data()
	delay_inp = None

	window = Tk()
	
	window.title("WWCC Checker")
	window.iconbitmap(resource_path(r"assets/star.ico"))

	window.geometry("800x600")
	window.configure(bg = "#93beff")
	canvas = Canvas(
		window,
		bg = "#93beff",
		height = 600,
		width = 800,
		bd = 0,
		highlightthickness = 0,
		relief = "ridge")
	canvas.place(x = 0, y = 0)
	main_font = font.Font(family="Cuprum", size=14)

	background_img = PhotoImage(file = resource_path(f"assets/background.png"))
	background_label = Label(window, image=background_img)
	background_label.place(x=0, y=0, relwidth=1, relheight=1)

	cb = Checkbutton(
		window,
		bd = 2,
		bg = "#D4D4D4",
		selectcolor = "#64DE93",
		indicatoron = False,
		command = lambda: save_data(usr_inp.get(), pwd_inp.get())
	)

	cb.place(x=70, y=393, width=20, height=20)

	if CHECKED:
		cb.select()

	file_img = PhotoImage(file = resource_path(f"assets/file_button.png"))
	file_button = Button(
		image = file_img,
		borderwidth = 0,
		highlightthickness = 0,
		command = lambda: add_file(window),
		relief = "flat")

	file_button.place(
		x = 510, y = 166,
		width = 180,
		height = 50)

	delay_img = PhotoImage(file = resource_path(f"assets/delay_button.png"))
	delay_button = Button(
		image = delay_img,
		borderwidth = 0,
		highlightthickness = 0,
		command = lambda: set_delay(window, element=delay_inp),
		relief = "flat")

	delay_button.place(
		x = 510, y = 337,
		width = 180,
		height = 50)

	delay_inp_img = PhotoImage(file = resource_path(f"assets/short_textbox.png"))
	delay_inp_bg = Label(window, image=delay_inp_img)
	delay_inp_bg.place(x=510, y=278)

	delay_inp = Entry(
		font=main_font,
		justify="center",
		bd = 0,
		bg = "#d4d4d4",
		highlightthickness = 0)

	delay_inp.place(
		x = 540, y = 282,
		width = 130.0,
		height = 48)

	delay_label = Label(
		window,
		text = "Delay set to 0.5 sec",
		font = ('Cuprum', 10),
		fg = "#343434"
    )
	delay_label.place(x=550, y=390)

	usr_inp_img = PhotoImage(file = resource_path(f"assets/long_textbox.png"))
	usr_inp_bg = Label(window, image=usr_inp_img)
	usr_inp_bg.place(x=50, y=210)

	usr_inp = Entry(
		font=main_font,
		bd = 0,
		bg = "#d4d4d4",
		highlightthickness = 0)

	usr_inp.place(
		x = 80, y = 214,
		width = 240.0,
		height = 48)

	if CHECKED:
		usr_inp.insert(END, usr)

	pwd_inp_img = PhotoImage(file = resource_path(f"assets/long_textbox.png"))
	pwd_inp_bg = Label(window, image=pwd_inp_img)
	pwd_inp_bg.place(x=50, y=320)

	pwd_inp = Entry(
		font=main_font,
		bd = 0,
		bg = "#d4d4d4",
		show="*",
		highlightthickness = 0)

	pwd_inp.place(
		x = 80, y = 324,
		width = 240.0,
		height = 48)

	if CHECKED:
		pwd_inp.insert(END, pwd)

	val_img = PhotoImage(file = resource_path(f"assets/go.png"))
	val_button = Button(
		image = val_img,
		borderwidth = 0,
		highlightthickness = 0,
		command = lambda: run(window, usr_inp.get(), pwd_inp.get()),
		relief = "flat")

	val_button.place(
		x = 300, y = 457,
		width = 200,
		height = 50)

	error_label = Label(window, bg="#EEEEEE", fg="#ff5959", font=('Cuprum', 10))
	error_label.place(x=400, y=540, anchor="center")

	window.resizable(False, False)
	save(window, usr_inp, pwd_inp)

	window.mainloop()

main()